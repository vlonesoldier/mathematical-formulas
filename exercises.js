console.log(`Geometric series sum for a1 = 5, q = 12, to 4 is ${geometricSeriesSum(5, 12, 4)}`)
console.log(`Factorial of 10 is ${factorial(10)}`);
console.log(`Binomial coefficient for n = 8 and k = 6 is ${binomialCoefficient(8, 6)}`);
console.log(`Pascal's triangle for 10:`, pascalsTriangle(10));
console.log('Sieve of Eratosthenes less than 10000', sieveOfEratosthenes(10000));
verifyGoldbachConjecture(10000);

function geometricSeriesSum(firstTerm, commonRatio, numOfTerms) {

    if (commonRatio === 1) {
        return firstTerm * numOfTerms;
    } else {
        return firstTerm * (1 - Math.pow(commonRatio, numOfTerms)) / (1 - commonRatio);
    }
}

function factorial(n) {

    if (n === 0 || n === 1) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}

function binomialCoefficient(n, k) {

    if (k > n)
        return 0;
    if (k === 0 || k === n)
        return 1;
    else
        return binomialCoefficient(n - 1, k - 1) + binomialCoefficient(n - 1, k);
}

function pascalsTriangle(rows) {

    let coefficients = [];
    for (let i = 0; i < rows; i++) {

        coefficients[i] = [];
        for (let j = 0; j <= i; j++) {

            if (j === 0 || j === i) {
                coefficients[i][j] = 1;
            } else {
                coefficients[i][j] = coefficients[i - 1][j - 1] + coefficients[i - 1][j];
            }
        }
    }
    return coefficients;
}

function sieveOfEratosthenes(limit) {
    const primeNums = [];
    const isPrimeArr = new Array(limit).fill(true);

    for (let i = 2; i <= Math.sqrt(limit); i++) {

        if (isPrimeArr[i]) {
            for (let j = i * i; j < limit; j += i) {
                isPrimeArr[j] = false;
            }
        }
    }

    for (let i = 2; i < limit; i++) {

        if (isPrimeArr[i]) {
            primeNums.push(i);
        }
    }

    return primeNums;
}


function verifyGoldbachConjecture(limit) {

    const primeNums = sieveOfEratosthenes(limit);
    for (let i = 4; i < limit; i += 2) {

        let hasRepresentation = false;
        for (let j = 0; j < primeNums.length; j++) {

            if (primeNums.includes(i - primeNums[j])) {
                hasRepresentation = true;
                break;
            }
        }
        if (!hasRepresentation) {
            console.log("Goldbach's conjecture is false for less than", limit);
        }
    }
    console.log("Goldbach's conjecture is true for all even integers less than", limit);
}

