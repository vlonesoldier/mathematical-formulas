1. Find the sum of the terms of a geometric series.
2. Evaluate n!.
3. Evaluate binomial coefficients.
4. Print out Pascal’s triangle.
5. Use the sieve of Eratosthenes to find all primes less than 10000.
6. Verify Goldbach’s conjecture for all even integers less than 10000.